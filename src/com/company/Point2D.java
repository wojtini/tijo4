package com.company;

public class Point2D {
    public double x ;
    public double y ;
    Point2D(double x , double y){
        this.x= x;
        this.y=y;
    }

    public Point2D() {
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
