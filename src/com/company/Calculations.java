package com.company;

public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] point){
        double sumX = 0;
        double sumY = 0 ;
        for(Point2D points : point){
            sumX += points.x;
            sumY += points.y;
        }
       // System.out.print("X : "+sumX/point.length+ " Y : " +sumY/point.length + "\n") ;
        return new Point2D(sumX/point.length ,sumY/point.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint){
        double mass = 0;
        double x = 0;
        double y = 0;
        for( MaterialPoint2D materialPoint2D : materialPoint){
            x += materialPoint2D.getX() * materialPoint2D.getMass() ;
            y += materialPoint2D.getY() * materialPoint2D.getMass() ;
            mass += materialPoint2D.getMass() ;
        }

        return new MaterialPoint2D(x/mass , y/mass , mass);
    }
}
